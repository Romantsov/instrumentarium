let a_a = document.querySelector('#AToneA');
let a_b = document.querySelector('#AToneB');
let a_c = document.querySelector('#AToneC');
let a_d = document.querySelector('#AToneD');
let a_e = document.querySelector('#AToneE');
let a_f = document.querySelector('#AToneF');
let a_g = document.querySelector('#AToneG');

const aToneList = {
    toneA: new Audio('../instrumentarium/assets/snd/andpipe/A_AP.mp3'),
    toneB: new Audio('../instrumentarium/assets/snd/andpipe/B_AP.mp3'),
    toneC: new Audio('../instrumentarium/assets/snd/andpipe/C_AP.mp3'),
    toneD: new Audio('../instrumentarium/assets/snd/andpipe/D_AP.mp3'),
    toneE: new Audio('../instrumentarium/assets/snd/andpipe/E_AP.mp3'),
    toneF: new Audio('../instrumentarium/assets/snd/andpipe/F_AP.mp3'),
    toneG: new Audio('../instrumentarium/assets/snd/andpipe/G_AP.mp3'),
};

aToneAll = [aToneList.toneA, aToneList.toneB, aToneList.toneC, aToneList.toneD, aToneList.toneE, aToneList.toneF, aToneList.toneG];

// function stopsound() {
//     for (var tone of toneAll) {
//         tone.pause();
//         tone.currentTime = 0;
//     }
// }

for (var t of c_list) {
    t.addEventListener('mouseover', function() {
        if (this === a_a) {
            aToneList.toneA.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneA.play();
        } else if (this === a_b) {
            aToneList.toneB.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneB.play();
        } else if (this === a_c) {
            aToneList.toneC.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneC.play();
        } else if (this === a_d) {
            aToneList.toneD.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneD.play();
        } else if (this === a_e) {
            aToneList.toneE.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneE.play();
        } else if (this === a_f) {
            aToneList.toneF.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneF.play();
        } else if (this === a_g) {
            aToneList.toneG.currentTime = 0;
            stopsound(aToneAll);
            aToneList.toneG.play();
        }})};