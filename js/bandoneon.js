let b_a = document.querySelector('#BToneA');
let b_b = document.querySelector('#BToneB');
let b_c = document.querySelector('#BToneC');
let b_d = document.querySelector('#BToneD');
let b_e = document.querySelector('#BToneE');
let b_f = document.querySelector('#BToneF');
let b_g = document.querySelector('#BToneG');
let b_h = document.querySelector('#BToneH');
let b_i = document.querySelector('#BToneI');
let b_j = document.querySelector('#BToneJ');
let b_k = document.querySelector('#BToneK');
let b_l = document.querySelector('#BToneL');
let b_m = document.querySelector('#BToneM');
let b_1 = document.querySelector('#BTone1');
let b_2 = document.querySelector('#BTone2');
let b_3 = document.querySelector('#BTone3');
let b_4 = document.querySelector('#BTone4');
let b_5 = document.querySelector('#BTone5');
let b_list = document.querySelectorAll('.tone');

const bTonelist = {
    toneA: new Audio('../instrumentarium/assets/snd/bandoneon/A_B.wav'),
    toneB: new Audio('../instrumentarium/assets/snd/bandoneon/B_B.wav'),
    toneC: new Audio('../instrumentarium/assets/snd/bandoneon/C_B.wav'),
    toneD: new Audio('../instrumentarium/assets/snd/bandoneon/D_B.wav'),
    toneE: new Audio('../instrumentarium/assets/snd/bandoneon/E_B.wav'),
    toneF: new Audio('../instrumentarium/assets/snd/bandoneon/F_B.wav'),
    toneG: new Audio('../instrumentarium/assets/snd/bandoneon/G_B.wav'),
    toneH: new Audio('../instrumentarium/assets/snd/bandoneon/H_B.wav'),
    toneI: new Audio('../instrumentarium/assets/snd/bandoneon/I_B.wav'),
    toneJ: new Audio('../instrumentarium/assets/snd/bandoneon/J_B.wav'),
    toneK: new Audio('../instrumentarium/assets/snd/bandoneon/K_B.wav'),
    toneL: new Audio('../instrumentarium/assets/snd/bandoneon/L_B.wav'),
    toneM: new Audio('../instrumentarium/assets/snd/bandoneon/M_B.wav'),
    tone1: new Audio('../instrumentarium/assets/snd/bandoneon/1_B.wav'),
    tone2: new Audio('../instrumentarium/assets/snd/bandoneon/2_B.wav'),
    tone3: new Audio('../instrumentarium/assets/snd/bandoneon/3_B.wav'),
    tone4: new Audio('../instrumentarium/assets/snd/bandoneon/4_B.wav'),
    tone5: new Audio('../instrumentarium/assets/snd/bandoneon/5_B.wav'),
};

bToneAll = [
    bTonelist.toneA, 
    bTonelist.toneB, 
    bTonelist.toneC, 
    bTonelist.toneD, 
    bTonelist.toneE, 
    bTonelist.toneF, 
    bTonelist.toneG,
    bTonelist.toneH,
    bTonelist.toneH,
    bTonelist.toneJ,
    bTonelist.toneK,
    bTonelist.toneL,
    bTonelist.toneM,
    bTonelist.tone1,
    bTonelist.tone2,
    bTonelist.tone3,
    bTonelist.tone4,
    bTonelist.tone5,
];

// function stopsound() {
//     for (var tone of toneAll) {
//         tone.pause();
//         tone.currentTime = 0;
//     }
// }

for (var t of b_list) {
    t.addEventListener('click', function() {
        function tSet(tone) {
                tone.currentTime = 0;
                stopsound(bToneAll);
                tone.play();
            };
        if (this === b_a) {
            tSet(bTonelist.toneA);
        } else if (this === b_b) {
            tSet(bTonelist.toneB);
        } else if (this === b_c) {
            tSet(bTonelist.toneC)
        } else if (this === b_d) {
            tSet(bTonelist.toneD);
        } else if (this === b_e) {
            tSet(bTonelist.toneE);
        } else if (this === b_f) {
            tSet(bTonelist.toneF);
        } else if (this === b_g) {
            tSet(bTonelist.toneG);
        } else if (this === b_h) {
            tSet(bTonelist.toneH);
        } else if (this === b_i) {
            tSet(bTonelist.toneI);
        } else if (this === b_j) {
            tSet(bTonelist.toneJ);
        } else if (this === b_k) {
            tSet(bTonelist.toneK);
        } else if (this === b_l) {
            tSet(bTonelist.toneL);
        } else if (this === b_m) {
            tSet(bTonelist.toneM);
        } else if (this === b_1) {
            tSet(bTonelist.tone1);
        } else if (this === b_2) {
            tSet(bTonelist.tone2);
        } else if (this === b_3) {
            tSet(bTonelist.tone3);
        } else if (this === b_4) {
            tSet(bTonelist.tone4);
        } else if (this === b_5) {
            tSet(bTonelist.tone5);
        }
    })};
