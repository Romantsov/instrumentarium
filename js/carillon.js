let c_a = document.querySelector('#CToneA');
let c_b = document.querySelector('#CToneB');
let c_c = document.querySelector('#CToneC');
let c_d = document.querySelector('#CToneD');
let c_e = document.querySelector('#CToneE');
let c_f = document.querySelector('#CToneF');
let c_g = document.querySelector('#CToneG');
let c_h = document.querySelector('#CToneH');
let c_i = document.querySelector('#CToneI');
let c_j = document.querySelector('#CToneJ');
let c_k = document.querySelector('#CToneK');
let c_l = document.querySelector('#CToneL');
let c_list = document.querySelectorAll('.tone')

const cTonelist = {
    toneA: new Audio('../instrumentarium/assets/snd/carillon/A_C.mp3'),
    toneB: new Audio('../instrumentarium/assets/snd/carillon/B_C.mp3'),
    toneC: new Audio('../instrumentarium/assets/snd/carillon/C_C.mp3'),
    toneD: new Audio('../instrumentarium/assets/snd/carillon/D_C.mp3'),
    toneE: new Audio('../instrumentarium/assets/snd/carillon/E_C.mp3'),
    toneF: new Audio('../instrumentarium/assets/snd/carillon/F_C.mp3'),
    toneG: new Audio('../instrumentarium/assets/snd/carillon/G_C.mp3'),
    toneH: new Audio('../instrumentarium/assets/snd/carillon/H_C.mp3'),
    toneI: new Audio('../instrumentarium/assets/snd/carillon/I_C.mp3'),
    toneJ: new Audio('../instrumentarium/assets/snd/carillon/J_C.mp3'),
    toneK: new Audio('../instrumentarium/assets/snd/carillon/K_C.mp3'),
    toneL: new Audio('../instrumentarium/assets/snd/carillon/L_C.mp3'),
    // toneM: new Audio('../instrumentarium/assets/snd/carillon/M_C.mp3'),
    // song: new Audio('../instrumentarium/assets/snd/carillon/carillon_song.mp3'),
};

cToneAll = [
    cTonelist.toneA,
    cTonelist.toneB,
    cTonelist.toneC,
    cTonelist.toneD,
    cTonelist.toneE,
    cTonelist.toneF,
    cTonelist.toneG,
    cTonelist.toneH,
    cTonelist.toneI,
    cTonelist.toneJ,
    cTonelist.toneK,
    cTonelist.toneL,
]

for (var t of c_list) {
    t.addEventListener('mouseenter', function(){
        if (this === c_a) {
            cTonelist.toneA.currentTime = 0;
            cTonelist.toneA.play();
        } else if (this === c_b) {
            cTonelist.toneB.currentTime = 0;
            cTonelist.toneB.play();
        } else if (this === c_c) {
            cTonelist.toneC.currentTime = 0;
            cTonelist.toneC.play();
        } else if (this === c_d) {
            cTonelist.toneD.currentTime = 0;
            cTonelist.toneD.play();
        } else if (this === c_e) {
            cTonelist.toneE.currentTime = 0;
            cTonelist.toneE.play();
        } else if (this === c_f) {
            cTonelist.toneF.currentTime = 0;
            cTonelist.toneF.play();
        } else if (this === c_g) {
            cTonelist.toneG.currentTime = 0;
            cTonelist.toneG.play();
        } else if (this === c_h) {
            cTonelist.toneH.currentTime = 0;
            cTonelist.toneH.play();
        } else if (this === c_i) {
            cTonelist.toneI.currentTime = 0;
            cTonelist.toneI.play();
        } else if (this === c_j) {
            cTonelist.toneJ.currentTime = 0;
            cTonelist.toneJ.play();
        } else if (this === c_k) {
            cTonelist.toneK.currentTime = 0;
            cTonelist.toneK.play();
        } else if (this === c_l) {
            cTonelist.toneL.currentTime = 0;
            cTonelist.toneL.play();
        } // else if (this === c_m) {
        //     cTonelist.toneM.currentTime = 0;
        //     cTonelist.toneM.play();
        // }
    })
};