const audioDTonePairs = ['A', 'B', 'C'].map(key => [
    document.querySelector(`#DTone${key}`),
    new Audio(`../instrumentarium/assets/snd/drum/${key}_D.wav`),
  ]);
  
for (const [toneElm, audio] of audioDTonePairs) {
    toneElm.addEventListener('click', () => audio.currentTime = 0);
    toneElm.addEventListener('click', () => audio.play());
}