let f_a = document.querySelector('#FToneA');
let f_b = document.querySelector('#FToneB');
let f_c = document.querySelector('#FToneC');
let f_d = document.querySelector('#FToneD');
let f_e = document.querySelector('#FToneE');
let f_f = document.querySelector('#FToneF');
let f_g = document.querySelector('#FToneG');
let f_h = document.querySelector('#FToneH');
let fNameList = [f_a, f_b, f_c, f_d, f_e, f_f, f_g, f_h];

let fToneA = new Audio('../instrumentarium/assets/snd/flute/A_F.wav');
let fToneB = new Audio('../instrumentarium/assets/snd/flute/B_F.wav');
let fToneC = new Audio('../instrumentarium/assets/snd/flute/C_F.wav');
let fToneD = new Audio('../instrumentarium/assets/snd/flute/D_F.wav');
let fToneE = new Audio('../instrumentarium/assets/snd/flute/E_F.wav');
let fToneF = new Audio('../instrumentarium/assets/snd/flute/F_F.wav');
let fToneG = new Audio('../instrumentarium/assets/snd/flute/G_F.wav');
let fToneH = new Audio('../instrumentarium/assets/snd/flute/H_F.wav');
let fToneList = [fToneA, fToneB, fToneC, fToneD, fToneE, fToneF, fToneG, fToneH];

// function stopsound(lst) {
//     for (var tone of lst) {
//         tone.pause();
//         tone.currentTime = 0;
//     }
// };

// omg this finally works properly
for (var nameIndex of fNameList) {
    nameIndex.addEventListener('mouseover', function() {
        for (var toneIndex of fToneList) {
            if (fNameList.indexOf(this) === fToneList.indexOf(toneIndex)) {
                toneIndex.currentTime = 0;
                stopsound(fToneList);
                toneIndex.play();
            }
        }
    })
};

// // Not using this here because I can't use stopsound() otherwise
// const audioFTonePairs = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'].map(key => [
//     document.querySelector(`#FTone${key}`), // First element of the pair
//     new Audio(`../instrumentarium/assets/snd/flute/${key}_F.wav`) // Second element of the pair
// ]);
  
// for (const [toneElm, audio] of audioFTonePairs) {
//     toneElm.addEventListener('mouseover', () => audio.currentTime = 0);
//     toneElm.addEventListener('mouseover', () => audio.play())
// };