let g_a1 = document.querySelector('#GToneA1');
let g_a2 = document.querySelector('#GToneA2');
let g_a3 = document.querySelector('#GToneA3');
let g_a4 = document.querySelector('#GToneA4');
let g_a5 = document.querySelector('#GToneA5');
let g_a6 = document.querySelector('#GToneA6');
let g_b1 = document.querySelector('#GToneB1');
let g_b2 = document.querySelector('#GToneB2');
let g_b3 = document.querySelector('#GToneB3');
let g_b4 = document.querySelector('#GToneB4');
let g_b5 = document.querySelector('#GToneB5');
let g_b6 = document.querySelector('#GToneB6');
let g_c1 = document.querySelector('#GToneC1');
let g_c2 = document.querySelector('#GToneC2');
let g_c3 = document.querySelector('#GToneC3');
let g_c4 = document.querySelector('#GToneC4');
let g_c5 = document.querySelector('#GToneC5');
let g_c6 = document.querySelector('#GToneC6');
let g_d1 = document.querySelector('#GToneD1');
let g_d2 = document.querySelector('#GToneD2');
let g_d3 = document.querySelector('#GToneD3');
let g_d4 = document.querySelector('#GToneD4');
let g_d5 = document.querySelector('#GToneD5');
let g_d6 = document.querySelector('#GToneD6');
let g_list = document.querySelectorAll('.tone');

let gstring1 = document.querySelector('#GString1');
let gstring2 = document.querySelector('#GString2');
let gstring3 = document.querySelector('#GString3');
let gstring4 = document.querySelector('#GString4');
let gstring5 = document.querySelector('#GString5');
let gstring6 = document.querySelector('#GString6');

const gTonelist = {
    toneA1: new Audio('../instrumentarium/assets/snd/guitar/A1.mp3'),
    toneA2: new Audio('../instrumentarium/assets/snd/guitar/A2.mp3'),
    toneA3: new Audio('../instrumentarium/assets/snd/guitar/A3.mp3'),
    toneA4: new Audio('../instrumentarium/assets/snd/guitar/A4.mp3'),
    toneA5: new Audio('../instrumentarium/assets/snd/guitar/A5.mp3'),
    toneA6: new Audio('../instrumentarium/assets/snd/guitar/A6.mp3'),
    toneB1: new Audio('../instrumentarium/assets/snd/guitar/B1.mp3'),
    toneB2: new Audio('../instrumentarium/assets/snd/guitar/B2.mp3'),
    toneB3: new Audio('../instrumentarium/assets/snd/guitar/B3.mp3'),
    toneB4: new Audio('../instrumentarium/assets/snd/guitar/B4.mp3'),
    toneB5: new Audio('../instrumentarium/assets/snd/guitar/B5.mp3'),
    toneB6: new Audio('../instrumentarium/assets/snd/guitar/B6.mp3'),
    toneC1: new Audio('../instrumentarium/assets/snd/guitar/C1.mp3'),
    toneC2: new Audio('../instrumentarium/assets/snd/guitar/C2.mp3'),
    toneC3: new Audio('../instrumentarium/assets/snd/guitar/C3.mp3'),
    toneC4: new Audio('../instrumentarium/assets/snd/guitar/C4.mp3'),
    toneC5: new Audio('../instrumentarium/assets/snd/guitar/C5.mp3'),
    toneC6: new Audio('../instrumentarium/assets/snd/guitar/C6.mp3'),
    toneD1: new Audio('../instrumentarium/assets/snd/guitar/D1.mp3'),
    toneD2: new Audio('../instrumentarium/assets/snd/guitar/D2.mp3'),
    toneD3: new Audio('../instrumentarium/assets/snd/guitar/D3.mp3'),
    toneD4: new Audio('../instrumentarium/assets/snd/guitar/D4.mp3'),
    toneD5: new Audio('../instrumentarium/assets/snd/guitar/D5.mp3'),
    toneD6: new Audio('../instrumentarium/assets/snd/guitar/D6.mp3'),
    // gSong: new Audio('../instrumentarium/assets/snd/guitar/guitar_song.mp3'),
}

for (var t of g_list) {
    t.addEventListener('mouseenter', function(){
        if (this === g_a1) {
            gTonelist.toneA1.currentTime = 0;
            gTonelist.toneA1.play();
            moveString(gstring1);
        } else if (this === g_a2) {
            gTonelist.toneA2.currentTime = 0;
            gTonelist.toneA2.play();
            moveString(gstring2);
        } else if (this === g_a3) {
            gTonelist.toneA3.currentTime = 0;
            gTonelist.toneA3.play();
            moveString(gstring3);
        } else if (this === g_a4) {
            gTonelist.toneA4.currentTime = 0;
            gTonelist.toneA4.play();
            moveString(gstring4);
        } else if (this === g_a5) {
            gTonelist.toneA5.currentTime = 0;
            gTonelist.toneA5.play();
            moveString(gstring5);
        } else if (this === g_a6) {
            gTonelist.toneA6.currentTime = 0;
            gTonelist.toneA6.play();
            moveString(gstring6);
        } else if (this === g_b1) {
            gTonelist.toneB1.currentTime = 0;
            gTonelist.toneB1.play();
            moveString(gstring1);
        } else if (this === g_b2) {
            gTonelist.toneB2.currentTime = 0;
            gTonelist.toneB2.play();
            moveString(gstring2);
        } else if (this === g_b3) {
            gTonelist.toneB3.currentTime = 0;
            gTonelist.toneB3.play();
            moveString(gstring3);
        } else if (this === g_b4) {
            gTonelist.toneB4.currentTime = 0;
            gTonelist.toneB4.play();
            moveString(gstring4);
        } else if (this === g_b5) {
            gTonelist.toneB5.currentTime = 0;
            gTonelist.toneB5.play();
            moveString(gstring5);
        } else if (this === g_b6) {
            gTonelist.toneB6.currentTime = 0;
            gTonelist.toneB6.play();
            moveString(gstring6);
        } else if (this === g_c1) {
            gTonelist.toneC1.currentTime = 0;
            gTonelist.toneC1.play();
            moveString(gstring1);
        } else if (this === g_c2) {
            gTonelist.toneC2.currentTime = 0;
            gTonelist.toneC2.play();
            moveString(gstring2);
        } else if (this === g_c3) {
            gTonelist.toneC3.currentTime = 0;
            gTonelist.toneC3.play();
            moveString(gstring3);
        } else if (this === g_c4) {
            gTonelist.toneC4.currentTime = 0;
            gTonelist.toneC4.play();
            moveString(gstring4);
        } else if (this === g_c5) {
            gTonelist.toneC5.currentTime = 0;
            gTonelist.toneC5.play();
            moveString(gstring5);
        } else if (this === g_c6) {
            gTonelist.toneC6.currentTime = 0;
            gTonelist.toneC6.play();
            moveString(gstring6);
        } else if (this === g_d1) {
            gTonelist.toneD1.currentTime = 0;
            gTonelist.toneD1.play();
            moveString(gstring1);
        } else if (this === g_d2) {
            gTonelist.toneD2.currentTime = 0;
            gTonelist.toneD2.play();
            moveString(gstring2);
        } else if (this === g_d3) {
            gTonelist.toneD3.currentTime = 0;
            gTonelist.toneD3.play();
            moveString(gstring3);
        } else if (this === g_d4) {
            gTonelist.toneD4.currentTime = 0;
            gTonelist.toneD4.play();
            moveString(gstring4);
        } else if (this === g_d5) {
            gTonelist.toneD5.currentTime = 0;
            gTonelist.toneD5.play();
            moveString(gstring5);
        } else if (this === g_d6) {
            gTonelist.toneD6.currentTime = 0;
            gTonelist.toneD6.play();
            moveString(gstring6);
        } 
})};

function moveString(el) {
    cancelEvent();
    el.style.backgroundColor = "rgba(145, 145, 145, 0.5)";
    var curbottom = el.style.bottom;
    console.log(el.style.bottom);
    var newbottom = parseInt(curbottom.slice(0, 1)) - 0.25;
    el.style.bottom = newbottom + '%';
    el.style.height = "6px";
    var timedEvent = window.setTimeout(function(){
        el.style.backgroundColor = "rgb(145, 145, 145)";
        el.style.bottom = curbottom + '%"';
        el.style.height = "2px";
    }, 2000);
    function cancelEvent() {
        window.clearTimeout(timedEvent);
    }
};