let HMAll = document.querySelectorAll('.tone');
const audioHMTonePairs = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'].map(key => [
    document.querySelector(`#HMTone${key}`), // First element of the pair
    new Audio(`../instrumentarium/assets/snd/harmonica/${key}_HA.wav`) // Second element of the pair
]);
  
// function stopsound() {
//     for (var tone of toneAll) {
//         tone.pause();
//         tone.currentTime = 0;
//     }
// }

var hmToneList = [
    new Audio('../instrumentarium/assets/snd/harmonica/A_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/B_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/C_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/D_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/E_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/F_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/G_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/H_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/I_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/G_HA.wav'),
    new Audio('../instrumentarium/assets/snd/harmonica/J_HA.wav'),
];

for (const [toneElm, audio] of audioHMTonePairs) {
    // toneElm.addEventListener('mouseenter', () => stopsound());
    toneElm.addEventListener('mouseover', () => audio.currentTime = 0);
    toneElm.addEventListener('mouseover', () => audio.play());
};

