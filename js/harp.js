const audioHTonePairs = ['A', 'B', 'C', 'D', 'E'].map(key => [
    document.querySelector(`#HTone${key}`),
    new Audio(`../instrumentarium/assets/snd/harp/${key}_H.mp3`),
    document.querySelector(`#HString${key}`),
  ]);
  
  for (const [toneElm, audio, string] of audioHTonePairs) {
    toneElm.addEventListener('mouseover', () =>  audio.currentTime = 0);
    toneElm.addEventListener('mouseover', () =>  audio.play());
    toneElm.addEventListener('mouseover', () =>  moveStringH(string));
  }

function moveStringH(el) {
    cancelEvent;
    el.style.backgroundColor = "rgba(145, 145, 145, 0.5)";
    el.style.width = "6px";
    var timedEvent = window.setTimeout(function(){
        el.style.backgroundColor = "rgb(145, 145, 145)";
        el.style.width = "2px";
    }, 2000);
    function cancelEvent() {
        window.clearTimeout(timedEvent);
}
};