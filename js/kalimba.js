let k_a = document.querySelector('#KToneA');
let k_b = document.querySelector('#KToneB');
let k_c = document.querySelector('#KToneC');
let k_d = document.querySelector('#KToneD');
let k_e = document.querySelector('#KToneE');
let k_list = [k_a, k_b, k_c, k_d, k_e];

const kTonelist = {
    toneA: new Audio('../instrumentarium/assets/snd/kalimba/A_K.mp3'),
    toneB: new Audio('../instrumentarium/assets/snd/kalimba/B_K.mp3'),
    toneC: new Audio('../instrumentarium/assets/snd/kalimba/C_K.mp3'),
    toneD: new Audio('../instrumentarium/assets/snd/kalimba/D_K.mp3'),
    toneE: new Audio('../instrumentarium/assets/snd/kalimba/E_K.mp3'),
    // song: new Audio('../instrumentarium/assets/snd/kalimba/kalimba_song.mp3'),
};

// let toneAll = [tonelist.toneA, tonelist.toneB, tonelist.toneC, tonelist.toneD, tonelist.toneE];

for (var t of k_list) {
    t.addEventListener('mouseenter', function(){
        if (this === k_a) {
            kTonelist.toneA.currentTime = 0;
            kTonelist.toneA.play();
        } else if (this === k_b) {
            kTonelist.toneB.currentTime = 0;
            kTonelist.toneB.play();
        } else if (this === k_c) {
            kTonelist.toneC.currentTime = 0;
            kTonelist.toneC.play();
        } else if (this === k_d) {
            kTonelist.toneD.currentTime = 0;
            kTonelist.toneD.play();
        } else if (this === k_e) {
            kTonelist.toneE.currentTime = 0;
            kTonelist.toneE.play();
        } 
    })
    // t.addEventListener('mouseleave', function(){
    //     for (var tone of toneAll) {
    //         tone.pause();
    //     }
    // })
};