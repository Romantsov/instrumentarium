// Sorry for the mess, I tried my best
const instr = {
    all: document.getElementsByClassName('instruments'),
    // all: ['guitar', 'carlion', 'kalimba', etc.],
    andpipe: document.getElementById('andpipe'),
    bandoneon: document.getElementById('bandoneon'),
    carillon: document.getElementById('carillon'),
    drum: document.getElementById('drums'),
    flute: document.getElementById('flute'),
    guitar: document.getElementById('guitar'),
    harmonica: document.getElementById('harmonica'),
    harp: document.getElementById('harp'),
    kalimba: document.getElementById('kalimba'),
    piano: document.getElementById('piano'),
    xylophone: document.getElementById('xylophone'),
    // other...
};

const popups = {
    all: document.getElementsByClassName('popup'),
    guitar: document.getElementById('popg'),
    carillon: document.getElementById('popc'),
    kalimba: document.getElementById('popk'),
    andpipe: document.getElementById('popa'),
    bandoneon: document.getElementById('popb'),
    drum: document.getElementById('popd'),
    flute: document.getElementById('popf'),
    harmonica: document.getElementById('pophm'),
    harp: document.getElementById('poph'),
    piano: document.getElementById('popp'),
    xylophone: document.getElementById('popx'),
};

const songs = {
    andpipe: new Audio('../instrumentarium/assets/snd/_songs/andpipe_song.mp3'),
    bandoneon: new Audio('../instrumentarium/assets/snd/_songs/bandoneon_song.mp3'),
    carillon: new Audio('../instrumentarium/assets/snd/_songs/carillon_song.mp3'),
    drum: new Audio('../instrumentarium/assets/snd/_songs/drum_song.mp3'),
    flute: new Audio('../instrumentarium/assets/snd/_songs/flute_song.mp3'),
    guitar: new Audio('../instrumentarium/assets/snd/_songs/guitar_song.mp3'),
    harmonica: new Audio('../instrumentarium/assets/snd/_songs/harmonica_song.mp3'),
    harp: new Audio('../instrumentarium/assets/snd/_songs/harp_song.mp3'),
    kalimba: new Audio('../instrumentarium/assets/snd/_songs/kalimba_song.mp3'),
    piano: new Audio('../instrumentarium/assets/snd/_songs/piano_song.mp3'),
    xylophone: new Audio('../instrumentarium/assets/snd/_songs/xylophone_song.mp3'),
};

const closeWindow = document.getElementsByClassName('close-window'); // for closing popups
const btnLights = document.querySelector('.lights'); // currently disabled
const mainWindow = document.querySelector('.main');
const playMusicAll = document.querySelectorAll('.music')
var musicOn = false;
let lights = false; // currently disabled

const musicLst = [
    'andpipe_song', 
    'bandoneon_song', 
    'carillon_song', 
    'drum_song', 
    'flute_song', 
    'guitar_song',
    'harmonica_song',
    'harp_song',
    'kalimba_song',
    'piano_song',
    'xylophone_song',
].map(key => [
    document.querySelector(`#bg-music-${key}`),
    new Audio(`../instrumentarium/assets/snd/_songs/${key}.mp3`),
])

const noiseLst = [
    'castanet_noise',
    'grelot_noise',
    'maracas_noise',
    'triangle_noise',
    'red_bell_noise',
    'blue_bell_noise',
    'green_bell_noise',
    'yellow_bell_noise',
].map(key => [
    document.querySelector(`#${key}`),
    new Audio(`../instrumentarium/assets/snd/_songs/${key}.wav`),
])

// Functions
// I'm so sorry... I was too tired to think.
instr.andpipe.addEventListener('mouseenter', function () {
    songs.andpipe.currentTime = 0;
    songs.andpipe.play();
});
instr.andpipe.addEventListener('mouseleave', function () {
    songs.andpipe.pause();
    songs.andpipe.currentTime = 0;
});
instr.bandoneon.addEventListener('mouseenter', function () {
    songs.bandoneon.play();
});
instr.bandoneon.addEventListener('mouseleave', function () {
    songs.bandoneon.pause();
    songs.bandoneon.currentTime = 0;
});
instr.carillon.addEventListener('mouseenter', function () {
    songs.carillon.play();
});
instr.carillon.addEventListener('mouseleave', function () {
    songs.carillon.pause();
    songs.carillon.currentTime = 0;
});
instr.drum.addEventListener('mouseenter', function () {
    songs.drum.play();
});
instr.drum.addEventListener('mouseleave', function () {
    songs.drum.pause();
    songs.drum.currentTime = 0;
});
instr.flute.addEventListener('mouseenter', function () {
    songs.flute.play();
});
instr.flute.addEventListener('mouseleave', function () {
    songs.flute.pause();
    songs.flute.currentTime = 0;
});
instr.guitar.addEventListener('mouseenter', function () {
    songs.guitar.currentTime = 0;
    songs.guitar.play();
});
instr.guitar.addEventListener('mouseleave', function () {
    songs.guitar.pause();
    songs.guitar.currentTime = 8;
});
instr.harmonica.addEventListener('mouseenter', function () {
    songs.harmonica.play();
});
instr.harmonica.addEventListener('mouseleave', function () {
    songs.harmonica.pause();
    songs.harmonica.currentTime = 0;
});
instr.harp.addEventListener('mouseover', function () {
    songs.harp.play();
});
instr.harp.addEventListener('mouseleave', function () {
    songs.harp.pause();
    songs.harp.currentTime = 0;
});
instr.kalimba.addEventListener('mouseenter', function () {
    songs.kalimba.play();
});
instr.kalimba.addEventListener('mouseleave', function () {
    songs.kalimba.pause();
    songs.kalimba.currentTime = 0;
});
instr.piano.addEventListener('mouseenter', function () {
    songs.piano.play();
});
instr.piano.addEventListener('mouseleave', function () {
    songs.piano.pause();
    songs.piano.currentTime = 0;
});
instr.xylophone.addEventListener('mouseenter', function () {
    songs.xylophone.play();
});
instr.xylophone.addEventListener('mouseleave', function () {
    songs.xylophone.pause();
    songs.xylophone.currentTime = 0;
});

// might as well do something similar for the huge instr.(instrument).addEventListener part above, I'm just too exhausted at this point
// I want to be done with this job already.
// Basically, this does the same thing that I wrote above, but for the non-instruments (bells, triangle, etc.)
for (const [instr, audio] of noiseLst) {
    instr.addEventListener('mouseenter', () => audio.currentTime = 0);
    instr.addEventListener('mouseenter', () => audio.play());
    instr.addEventListener('mouseleave', () => audio.pause());
    instr.addEventListener('mouseleave', () => audio.currentTime = 0);
}

// This part is for playing/stopping music when clicking the melody button in popups
for (const [button, audio] of musicLst) {
    if (musicOn == false) {
        button.addEventListener('click', () => audio.currentTime = 0);
        button.addEventListener('click', () => audio.play());
        button.addEventListener('click', () => button.style.backgroundColor = '#f66');
        // button.addEventListener('click', function() { musicOn = !musicOn }); // most likely because of that?
    }
    // } else if (musicOn == true) { // Doesn't work somehow smh
    //     button.addEventListener('click', () => audio.pause());
    //     button.addEventListener('click', () => audio.currentTime = 0);
    //     button.addEventListener('click', () => musicOn = !musicOn);
    //     button.addEventListener('click', () => button.style.backgroundColor = "#a66");
    // }
};

// A bit inconvenient, but this basically works with the one above... I know, it's... odd.
// But hey, the `else if (musicOn == true)` part didn't wanna work for some god knows why reason.
for (var button of playMusicAll) {
    button.addEventListener('click', function () {
        if (musicOn == false) {
            // // This doesn't wanna work here, just so you know that I tried that. Give it a try if still curious.
            // for (const [button, audio] of musicLst) {
            //     button.addEventListener('click', () => audio.currentTime = 0);
            //     button.addEventListener('click', () => audio.play());
            //     button.addEventListener('click', () => button.style.backgroundColor = '#f66');
            // }
            musicOn = !musicOn; // Well, at least it works here
        } else {
            for (var btn of playMusicAll) {
                btn.style.backgroundColor = "#69f";
                for (const [button, audio] of musicLst) {
                    audio.pause();
                    audio.currentTime = 0;
                    musicOn = !musicOn;
                }
            }
            }
        }
    )
};

// Instrument panel pop-ups
// Sorry for the mess, I made this at the very start
for (let i of instr.all) {
    i.addEventListener('click', function() {
        // Hides the main menu
        function hideWindow() {
            mainWindow.style.display = 'none';
        };
        if (i === instr.guitar) {
            popup(popups.guitar);
            hideWindow();
        };
        if (i === instr.carillon) {
            popup(popups.carillon);
            hideWindow();
        };
        if (i === instr.kalimba) {
            popup(popups.kalimba);
            hideWindow();
        };
        if (i === instr.andpipe) {
            popup(popups.andpipe);
            hideWindow();
        };
        if (i === instr.bandoneon) {
            popup(popups.bandoneon);
            hideWindow();
        };
        if (i === instr.drum) {
            popup(popups.drum);
            hideWindow();
        };
        if (i === instr.flute) {
            popup(popups.flute);
            hideWindow();
        };
        if (i === instr.harmonica) {
            popup(popups.harmonica);
            hideWindow();
        };
        if (i === instr.harp) {
            popup(popups.harp);
            hideWindow();
        };
        if (i === instr.piano) {
            popup(popups.piano);
            hideWindow();
        };
        if (i === instr.xylophone) {
            popup(popups.xylophone);
            hideWindow();
        };
    })
};

// for popping up instrument panels
function popup(el) {
    el.style.display = 'block';
    for (const [button, audio] of musicLst) {
        button.backgroundColor = "#69f";
        audio.pause();
        audio.currentTime = 0;
        musicOn = false;
    }
};

// Self-explanatory. Used in other files as well.
function stopsound(lst) {
    for (var tone of lst) {
        tone.pause();
        tone.currentTime = 0;
    }
};

// for closing instrument panels (and stopping sounds)
for (var el of closeWindow) {
    el.addEventListener('click', function() {
        stopsound(fToneList); // from flute.js, carillon.js, andpipe.js and harmonica.js
        stopsound(cToneAll);
        stopsound(aToneAll);
        stopsound(bToneAll);
        stopsound(hmToneList); // Doesn't work, but it's ok... and sorry for the mess again.
        for (var i of popups.all) {
            i.style.display = 'none';
            mainWindow.style.display = 'block';
            musicOn = false;
            for (const [button, audio] of musicLst) {
                button.backgroundColor = "#69f";
                audio.pause();
                audio.currentTime = 0;
        }
        }
    })
};

// // Lights on/off logic
// function light() {
//     if (lights == true) {
//         document.getElementById('maindiv').style.backgroundImage = 'url("../instrumentarium/assets/img/instrumentarium_offon.png")'
//     }
//     else {
//         document.getElementById('maindiv').style.backgroundImage = 'url("../instrumentarium/assets/img/instrumentarium.png")'
//     }
// };

// // Logic for enabling/disabling lights that uses the light() function (the one above)
// btnLights.addEventListener('click', function(){
//     if (lights === true) {
//         lights = false;
//     } else {
//         lights = true;
//     }
//     light();
// });