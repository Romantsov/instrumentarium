// pNameList = [
//     document.querySelector('#PToneA'),
//     document.querySelector('#PToneA1'),
//     document.querySelector('#PToneB'),
//     document.querySelector('#PToneC'),
//     document.querySelector('#PToneC1'),
//     document.querySelector('#PToneD'),
//     document.querySelector('#PToneD1'),
//     document.querySelector('#PToneE'),
//     document.querySelector('#PToneF'),
//     document.querySelector('#PToneF1'),
//     document.querySelector('#PToneG'),
//     document.querySelector('#PToneG1'),
//     document.querySelector('#PToneH'),
//     document.querySelector('#PToneH1'),
//     document.querySelector('#PToneI'),
// ];

// pToneList = [
//     new Audio('../instrumentarium/assets/snd/piano/A_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/A1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/B_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/C_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/C1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/D_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/D1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/E_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/F_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/F1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/G_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/G1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/H_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/H1_P.mp3'),
//     new Audio('../instrumentarium/assets/snd/piano/I_P.mp3'),
// ];

// for (var nameIndex of pNameList) {
//     nameIndex.addEventListener('mouseover', function() {
//         for (var toneIndex of pToneList) {
//             if (pNameList.indexOf(nameIndex) === pToneList.indexOf(toneIndex)) {
//                 toneIndex.currentTime = 0;
//                 toneIndex.play();
//             }
//         }
//     })
// };

const audioPTonePairs = ['A', 'A1', 'B', 'C', 'C1', 'D', 'D1', 'E', 'F', 'F1', 'G', 'G1', 'H', 'H1', 'I'].map(key => [
    document.querySelector(`#PTone${key}`),
    new Audio(`../instrumentarium/assets/snd/piano/${key}_P.mp3`),
  ]);
  
for (const [toneElm, audio] of audioPTonePairs) {
    toneElm.addEventListener('mouseover', () => audio.currentTime = 0);
    toneElm.addEventListener('mouseover', () => audio.play());
}