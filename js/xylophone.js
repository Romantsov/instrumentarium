const audioXTonePairs = ['1', '2', '3', '4', '5', '6', '7', '8'].map(key => [
    document.querySelector(`#XTone${key}`),
    new Audio(`../instrumentarium/assets/snd/xylophone/Db${key}.mp3`),
  ]);
  
for (const [toneElm, audio] of audioXTonePairs) {
    toneElm.addEventListener('mouseover', () => audio.currentTime = 0);
    toneElm.addEventListener('mouseover', () => audio.play());
};